import { Component, Input } from '@angular/core';

@Component({
  selector: 'light-button',
  templateUrl: 'LightButton.template.html'
})
export class LightButton {

  @Input() numberOfLights : number;
  @Input() isBottom : boolean;
  @Input() backGround: string;
  lights : any[] = [];
  height : number;

  ngOnInit(){
    for(var i = 0 ; i < this.numberOfLights; i++){
      this.lights.push('');
      this.height = 100/this.numberOfLights;
    }
  }
}