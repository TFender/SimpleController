import { Component, Input } from '@angular/core';

@Component({
  selector: 'arrow-button',
  templateUrl: 'ArrowButton.template.html'
})
export class ArrowButton {

    @Input() isBottom: boolean;
    @Input() backGround: string
    
    private styleString: string;

    ngOnInit(){
        this.styleString = (this.isBottom ? '90' : '270');
    }
}
