import { Component, Input } from '@angular/core';

@Component({
  selector: 'big-button',
  templateUrl: 'BigButton.template.html'
})
export class BigButton {

  @Input() isBottom: boolean
  @Input() backGround: string
  private styleString: string; 
  isClicked: boolean = false;

  ngOnInit(){
    this.styleString = (this.isBottom ? '50%' : '0%');
  }
  
  active() {
    this.isClicked = true; 
  }

  inactive() {
    this.isClicked = false; 
  }

}
