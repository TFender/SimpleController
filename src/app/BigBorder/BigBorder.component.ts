import { Component, Input } from '@angular/core';

@Component({
  selector: 'big-border',
  templateUrl: 'BigBorder.template.html'
})
export class BigBorder {
  @Input() backGround: string;
}
