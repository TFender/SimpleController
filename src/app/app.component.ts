import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private screenOrientation: ScreenOrientation, private androidFullScreen: AndroidFullScreen) {
    platform.ready().then(() => {

      this.androidFullScreen.isImmersiveModeSupported()
        .then(() => {
          console.log('Immersive mode supported');
          this.androidFullScreen.immersiveMode();
        })
        .catch(err => console.log(err));

      console.log(screenOrientation.ORIENTATIONS.PORTRAIT);
      screenOrientation.lock(screenOrientation.ORIENTATIONS.PORTRAIT).then(function success() {
        console.log("Successfully locked the orientation");
    }, function error(errMsg) {
        console.log("Error locking the orientation :: " + errMsg);
    });
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

